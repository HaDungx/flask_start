from flask import Flask, request, jsonify


app = Flask(__name__)

@app.route('/chat', methods = ['GET'])
def chat():
  if request.method == 'GET':
    return jsonify({'key':'value'})
  else:
    raise Exception()  #TODO specific error for `unsupported method` later

if __name__ == '__main__':
    app.run(debug=True)  #TODO Dung add param port=1031 as today date
